let app = {
  roomList: [],
  userLogged: '',
  userCoins: 10,
  rentedRooms: [],

  init: function () {
    login.init();
    app.loadRooms();
  },

  loadRooms: function () {
    $.ajax({
      method: "GET",
      url: "/data/rooms.json"
    }).done(app.onLoadRoomsSuccess).fail(app.onError);
  },

  onLoadRoomsSuccess: function (rooms) {
    app.roomList = rooms;
    app.ui.displayRooms();
  },

  onError: function (err) {
    alert('Error, please open the console to match it');
    console.log('Error on AJAX Call: ', err.status);
  },

  eventsHandler: function () {
    const buttonsRooms = $(".btnRoom"),
          btnLogOut    = $(".logout");

    buttonsRooms.on('click', function () {
      let nameRoom  = $(this).data('room'),
          coinsRoom = $(this).data('cost');

      app.ui.updateUserDetails(nameRoom, coinsRoom);
    });

    btnLogOut.on('click', function () {
      login.doLogout()
    });
  },

  ui: {
    displayUser: function () {
      const boardBookedRooms = $("#bookingList");
      let userh3     = $(document.createElement('h3')),
          coinsUser  = $(document.createElement('p')),
          btnLogOut  = $(document.createElement('button')),
          listRented = $(document.createElement('ul'));

      userh3.text('Welcome back, ' + app.userLogged)
        .appendTo(boardBookedRooms);

      coinsUser.text('You\'ve ' + app.userCoins + ' coins!')
        .addClass('user-coins')
        .appendTo(boardBookedRooms);

      btnLogOut.text('Reset everything')
        .addClass('logout')
        .appendTo(boardBookedRooms);

      listRented.addClass('listRented').appendTo(boardBookedRooms);

      this.displayRooms();
    },

    displayRooms: function () {
      const rooms        = app.roomList,
            boardOfRooms = $("#roomList"),
            ulRooms      = $(document.createElement('ul')),
            roomsh3      = $(document.createElement('h2'));

      boardOfRooms.html('');

      roomsh3.text('List of our Rooms').appendTo(boardOfRooms);
      ulRooms.appendTo(boardOfRooms);

      if (rooms.length > 1) {
        for (const room of rooms) {
          const liRoom     = $(document.createElement('li')),
                nameRoom   = $(document.createElement('span')),
                costRoom   = $(document.createElement('span')),
                btnAddRoom = $(document.createElement('button'));

          nameRoom.text(room.name);
          nameRoom.appendTo(liRoom);

          costRoom.text(' - cost: ' + room.cost);
          costRoom.appendTo(liRoom);

          btnAddRoom.text('Book this room')
            .attr({'data-room': room.name, 'data-cost': room.cost})
            .addClass('btnRoom');

          if (app.userLogged !== '') {
            btnAddRoom.appendTo(liRoom);
          }

          liRoom.appendTo(ulRooms);
        }
      }
      app.eventsHandler();
    },

    displayBookedRooms: function () {
      const listRented = $(".listRented");
      listRented.empty();

      app.rentedRooms.forEach(r => {
        const rentedRoom = $(document.createElement('li'));

        rentedRoom.text(r).appendTo(listRented);
      });
    },

    updateUserDetails: function (room, coins) {
      const displayedCoins = $(".user-coins");
      let newCoins = app.userCoins;

      if (newCoins >= coins) {
        newCoins = app.userCoins - coins;
        app.userCoins = newCoins;
        app.rentedRooms.push(room);

        const objUser = {
          "user": app.userLogged,
          "coins": app.userCoins,
          "rented": app.rentedRooms
        };

        app.ui.displayBookedRooms();
        sessionStorage.setItem('loggedUser', JSON.stringify(objUser));
      } else {
        alert('You don\'t have coins to book that room');
      }

      displayedCoins.text('You\'ve ' + newCoins + ' coins!')
    }
  }
};

let login = {

  init: function () {
    const sessionUser = JSON.parse(sessionStorage.getItem('loggedUser'));

    if (sessionStorage.getItem('loggedUser')) {
      app.userLogged = sessionUser.user;
      app.userCoins = sessionUser.coins;
      app.rentedRooms = sessionUser.rented;

      app.ui.displayUser();
      app.ui.displayBookedRooms();

      $("#formLogin").addClass('hidden');
    } else {
      this.eventsHandler();
    }
  },

  doLogin: function () {
    const form = $("#formLogin"),
          user = $("#user").val(),
          pass = $("#pass").val();

    if (user.length >= 1 && pass.length >= 1) {
      app.userLogged = user;
      app.ui.displayUser();
      form.addClass('hidden');

      const objUser = {
        "user": app.userLogged,
        "coins": app.userCoins,
        "rented": app.rentedRooms
      };

      sessionStorage.setItem('loggedUser', JSON.stringify(objUser));
    }
  },

  doLogout: function () {
    sessionStorage.clear();
    location.reload();
  },

  eventsHandler: function () {
    const btnLogin = $("#formLogin").find(".login");

    btnLogin.on('click', () => {
      this.doLogin();
    });
  }
};

$(document).ready(app.init);